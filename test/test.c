#include "elog.h"
#include <stdio.h>
#include <stdlib.h>

#define DAEMON_ADDR "localhost"
#define DAEMON_PORT 1234

int main(void)
{
	struct elog_t elog;
	if (!elog_init_logging(&elog, DAEMON_ADDR, DAEMON_PORT))
	{
		fprintf(stderr, "ERR: [elog_init_logging] Failed to connect to elog daemon.\n");
		return EXIT_FAILURE;
	}

	if (!elog_log(&elog, "debug system", ELOG_SEVERITY_DEBUG, "Test message", "{\"test payload\":5}"))
	{
		fprintf(stderr, "ERR: [elog_log] Failed to send message to elog daemon.\n");
		return EXIT_FAILURE;
	}

	if (!elog_logm(&elog, "test harness", ELOG_SEVERITY_DEBUG, "Test of message-only transmission"))
	{
		fprintf(stderr, "ERR: [elog_logm] Failed to send message to elog daemon.\n");
		return EXIT_FAILURE;		
	}

	if (!elog_logp(&elog, "test harness", ELOG_SEVERITY_DEBUG, "{\"payload-only\":\"test successful\"}"))
	{
		fprintf(stderr, "ERR: [elog_logp] Failed to send message to elog daemon.\n");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
