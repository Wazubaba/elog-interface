#ifndef ELOG_H
#define ELOG_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>

#define ELOG_MAGIC "ELOG0x0525234"
#define ELOG_MAGIC_LEN 13
#define ELOG_VERSION 1


struct elog_t
{
	int socketfd;
	struct sockaddr_in serv_addr;
};

enum elog_severity
{
	ELOG_SEVERITY_INFO,
	ELOG_SEVERITY_WARN,
	ELOG_SEVERITY_ERROR,
	ELOG_SEVERITY_CRITICAL,
	ELOG_SEVERITY_FATAL,
	ELOG_SEVERITY_DEBUG
};

/*
	Initialize the connection to the daemon.
	Returns 1 if successful, 0 if not.
*/
const unsigned char elog_init_logging(struct elog_t* self, const char* address, const unsigned short port);

/*
	Emit a message to the daemon.
	Returns 1 if successful, 0 if not.
*/
unsigned char elog_log(struct elog_t* self, const char* subsystem, const int severity, const char* message, const char* payload);
unsigned char elog_logm(struct elog_t* self, const char* subsystem, const int severity, const char* message);
unsigned char elog_logp(struct elog_t* self, const char* subsystem, const int severity, const char* payload);

/*
	This is really only needed for retardblows because you have to de/init their
	socket subsystem because god forbid they just use a bloody standard thing and
	stop inventing their own inferior alternatives...

	100% uncessary with a normal sane socket system. I inlined them to
	try to minimize uncessary bloat inherent in the function calls themselves.
*/
int elog_init(void);
int elog_deinit(void);
#endif // END OF ELOG_H

