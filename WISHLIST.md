~~Currently only Linux is confirmed to be working. I believe porting to
Windows will be a relatively painless affair (as much so as doing any
serious development for Windows can ever be).~~

~~Windows port process:~~
- [ ] ~~Windows port~~
	- [x] ~~Wrap in Windows headers~~
	- [x] ~~`ifdef` wrappers to redirect to the correct functions per platform~~
	- [ ] ~~Testing~~
	- [ ] ~~Alter build script if necessary~~

```
  ██████    ██ ▄████▄  ██ ▄█▀                        
▓██   ▒██  ▓██▒██▀ ▀█  ██▄█▒                         
▒████ ▓██  ▒██▒▓█    ▄▓███▄░                         
░▓█▒  ▓▓█  ░██▒▓▓▄ ▄██▓██ █▄                         
░▒█░  ▒▒█████▓▒ ▓███▀ ▒██▒ █▄                        
 ▒ ░  ░▒▓▒ ▒ ▒░ ░▒ ▒  ▒ ▒▒ ▓▒                        
 ░    ░░▒░ ░ ░  ░  ▒  ░ ░▒ ▒░                        
 ░ ░   ░░░ ░ ░░       ░ ░░ ░                         
         ░    ░ ░     ░  ░                           
 █     █░██▓███▄    █▓█████▄ ▒█████  █     █░ ██████ 
▓█░ █ ░█▓██▒██ ▀█   █▒██▀ ██▒██▒  ██▓█░ █ ░█▒██    ▒ 
▒█░ █ ░█▒██▓██  ▀█ ██░██   █▒██░  ██▒█░ █ ░█░ ▓██▄   
░█░ █ ░█░██▓██▒  ▐▌██░▓█▄   ▒██   ██░█░ █ ░█  ▒   ██▒
░░██▒██▓░██▒██░   ▓██░▒████▓░ ████▓▒░░██▒██▓▒██████▒▒
░ ▓░▒ ▒ ░▓ ░ ▒░   ▒ ▒ ▒▒▓  ▒░ ▒░▒░▒░░ ▓░▒ ▒ ▒ ▒▓▒ ▒ ░
  ▒ ░ ░  ▒ ░ ░░   ░ ▒░░ ▒  ▒  ░ ▒ ▒░  ▒ ░ ░ ░ ░▒  ░ ░
  ░   ░  ▒ ░  ░   ░ ░ ░ ░  ░░ ░ ░ ▒   ░   ░ ░  ░  ░  
    ░    ░          ░   ░       ░ ░     ░         ░  
                      ░                              
```

