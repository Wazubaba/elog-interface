# E L O G - interface library
## Introduction
`Elog` is a logging daemon that tries to achieve the same amount of
power as found in the Windows Event Viewer but with the ease-of-use
of Linux's Syslog api.

This project serves as the api interface to the logging system. The
logger itself is written in the [Nim][1] programming language but this
interface is written in C so as to achieve maximum possible portability.

Please bear in mind, this is not intended to be used for implementing
alternative logging servers, only clients.

For documentation, you can either refer to the [header file](/include/elog.h) (the api is
only 4 functions) or view the [api documentation](/doc/api.md).

[1]: https://nim-lang.org/

## Requirements
Currently, while ELog itself is cross-platform, this api only supports Linux.
See the [wishlist](WISHLIST.md) for more information.

## Compiling
See [this info](COMPILING.md).

## Future
See [this info](WISHLIST.md) for future plans.

