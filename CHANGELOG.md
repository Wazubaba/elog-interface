# 1.1.0 - Better support for inclusion
I've renamed `config.h` to `elog_config.h` so as to ease using this
library without relying on meson as the buildsystem.

# 1.0.0 - Initial version
No real comments here. This is the "we are already working for the most
part" release. Windows support will come eventually.

