#include "elog.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "elog_config.h"

#ifdef use_stderr
	#define emit fprintf
	#define pemit perror
	#define hemit herror
#else
	int fakefprintf(FILE* stream, const char* format, ...){return 0;}
	void fakeperror(const char* s){}
	void fakeherror(const char* s){}

	#define emit fakefprintf
	#define pemit fakeperror
	#define hemit fakeherror
#endif

const unsigned char _reconnect(struct elog_t* self)
{
	if ((self->socketfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		emit(stderr, "ERR: Failed to allocate socket\n");
		return 0;
	}

	int readLen;
	char recvBuffer[ELOG_MAGIC_LEN];
	char sendBuffer[ELOG_MAGIC_LEN];
	int versionBuffer;
	
	int magiclen = ELOG_MAGIC_LEN;

	memset(recvBuffer, '0', sizeof(recvBuffer));
	memset(sendBuffer, '0', sizeof(sendBuffer));


	int debug = connect(self->socketfd, (struct sockaddr *) &self->serv_addr, sizeof(self->serv_addr));
	if (debug < 0)
	{
		emit(stderr, "ERR: Failed to connect to elog daemon\n");
		emit(stderr, "ERR: debug is now %d\n", debug);
		pemit("Failed to connect");
		return 0;
	}

	strcpy(sendBuffer, ELOG_MAGIC);


	/* Send magic to confirm things */
	write(self->socketfd, &magiclen, sizeof(int));
	write(self->socketfd, sendBuffer, strlen(sendBuffer));

	/* Now wait for the server to echo back the magic */
	readLen = read(self->socketfd, recvBuffer, sizeof(recvBuffer));
	if (readLen < ELOG_MAGIC_LEN)
	{
		emit(stderr, "ERR: Invalid header length recieved from server\n");
		emit(stderr, "ERR: Header recieved is of length %d\n", readLen);
		return 0;
	}

	if (strncmp(recvBuffer, ELOG_MAGIC, ELOG_MAGIC_LEN) != 0)
	{
		emit(stderr, "ERR: Malformed header recieved\n");
		emit(stderr, "ERR: Recieved header: %s\n", recvBuffer);
		return 0;
	}

	/* The server will now send its version number */
	readLen = read(self->socketfd, &versionBuffer, sizeof(int));
	if (readLen < sizeof(int))
	{
		emit(stderr, "ERR: Malformed version identifier recieved\n");
		return 0;
	}

	if (versionBuffer > ELOG_VERSION)
	{
		emit(stderr, "ERR: Server version is newer than client\n");
		emit(stderr, "ERR: Version recieved from server is %d\n", versionBuffer);
		return 0;
	}

	return 1;
}

const unsigned char elog_init_logging(struct elog_t* self, const char* address, const unsigned short port)
{
	/* Initialize the socket and allocate all necessary fields within self reference. */
	self->socketfd = 0;

	char addr[100];
	unsigned char is_host = 0;

	size_t itr;
	for (itr = 0; itr < strlen(address); itr++)
	{
		if (is_host) break;
		switch (address[itr])
		{ /* TODO: Find a better way to do this... */
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '.':
			case ':': continue;
			default:
			{
				is_host = 1;
				break;
			}
		}
	}

	if (is_host)
	{
		struct hostent* he;
		he = gethostbyname(address);
		if (NULL == he)
		{
			hemit("ERR: Initialization failed");
			return 0;
		}
		struct in_addr** addr_list = (struct in_addr**) he->h_addr_list;

		/* Just grab the first IP for the given host */
		strcpy(addr, inet_ntoa(*addr_list[0]));
	}
	else
		strcpy(addr, address);

	self->serv_addr.sin_family = AF_INET;
	self->serv_addr.sin_port = htons(port);
	self->serv_addr.sin_addr.s_addr = inet_addr(addr);
	return 1;
}

unsigned char elog_log(struct elog_t* self, const char* subsystem, const int severity, const char* message, const char* payload)
{
	if (!_reconnect(self))
	{
		emit(stderr, "ERR: Failed to establish connection to elog daemon\n");
		return 0;
	}

	/*
		We've connected to the server by now, so start sending the various
		data. We have to dynamically allocate memory here because we have
		no way of knowning how long the message or payload will be.
	*/
	int subsystemSize, messageSize, payloadSize;

	subsystemSize = strlen(subsystem) * sizeof(char);
	messageSize = strlen(message) * sizeof(char);
	payloadSize = strlen(payload) * sizeof(char);

	int sendSize;

	sendSize = write(self->socketfd, &subsystemSize, sizeof(int));
	if (sendSize != sizeof(int))
	{
		emit(stderr, "ERR001: Failed to send %zu bytes", sizeof(int));
		return 0;
	}

	sendSize = write(self->socketfd, subsystem, subsystemSize);
	if (sendSize != subsystemSize)
	{
		emit(stderr, "ERR002: Failed to send %d bytes\n", subsystemSize);
		return 0;
	}

	sendSize = write(self->socketfd, &severity, sizeof(int));
	if (sendSize != sizeof(int))
	{
		emit(stderr, "ERR003: Failed to send %zu bytes", sizeof(int));
		return 0;
	}

	sendSize = write(self->socketfd, &messageSize, sizeof(int));
	if (sendSize != sizeof(int))
	{
		emit(stderr, "ERR004: Failed to send %zu bytes", sizeof(int));
		return 0;
	}

	sendSize = write(self->socketfd, message, messageSize);
	if (sendSize != messageSize)
	{
		emit(stderr, "ERR005: Failed to send %d bytes\n", messageSize);
		return 0;
	}

	sendSize = write(self->socketfd, &payloadSize, sizeof(int));
	if (sendSize != sizeof(int))
	{
		emit(stderr, "ERR006: Failed to send %zu bytes", sizeof(int));
		return 0;
	}

	sendSize = write(self->socketfd, payload, payloadSize);
	if (sendSize != payloadSize)
	{
		emit(stderr, "ERR007: Failed to send %d bytes\n", payloadSize);
		return 0;
	}

	/*
		We have to close the file-descriptor each time for some obtuse reason
		that I simply cannot seem to understand... ><
	*/
	shutdown(self->socketfd, SHUT_RDWR);
	close(self->socketfd);
	return 1;
}

unsigned char elog_logm(struct elog_t* self, const char* subsystem, const int severity, const char* message)
{
	return elog_log(self, subsystem, severity, message, "");
}

unsigned char elog_logp(struct elog_t* self, const char* subsystem, const int severity, const char* payload)
{
	return elog_log(self, subsystem, severity, "", payload);
}

