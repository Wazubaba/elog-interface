## Building as a library
This project uses the [Meson build system][1] to compile static/dynamic libs
and the test program.

For a quick example - execute `meson build` to generate a `build` directory,
then change to it and execute `ninja` to build everything. You can run
`ninja test` to run the unit-test. From this directory you can also change
settings. Currently the only offered option is to disable error message outputs.
See `meson configure` for a list of project-level options, or reference
[meson_options.txt](meson_options.txt) in the root project directory for more info.

[1]: http://mesonbuild.com/

## Wiring into your own projects
The elog interface is simple to wire into another project as it is
only a single header and source file. No special libraries are needed
to link with, so wiring it into your project without using meson is
very simple. Unless you are Windows where everything has to be 10x
more difficult for reasons, and you have to link in... I think
`Ws2_32`. IDK use cygwin for this tbqh...

Add a `elog_config.h` file in same directory as `elog.c` and add options
there. Currently the only option is `use_stderr`, which can be used like
this:
```c
#define use_stderr
```
If defined, information will be displayed in the event of connection issues,
otherwise it will stay quiet.

