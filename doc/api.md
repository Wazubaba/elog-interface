# elog api

## Initialization
```c
const unsigned char
elog_init_logging(struct elog_t* self, const char* address, const unsigned short port);
```
This function will initialize a struct containing information with connecting to
the `elog` daemon. Address can be either an IP or a hostname.

Returns `1` if initialization was successful, `0` if not. A message will be output
to `stdout` if a failure occurs unless configured not to do so.

```c
unsigned char
elog_log(struct elog_t* self, const char* subsystem, const int severity, const char* message, const char* payload);
```
This is the primary workhorse function that will emit an entry to the target
`elog` daemon. `severity` should be any of the `elog_severity` enum values.
Payload should either be a json value or an empty string. If it is invalid your
payload will be discarded.

Returns 1 if message was sent successfully, otherwise 0.

```c
unsigned char
elog_logm(struct elog_t* self, const char* subsystem, const int severity, const char* message);
```
This is a wrapper for `elog_log` which omits the `payload` field.

Returns 1 if message was sent successfully, otherwise 0.

```c
unsigned char
elog_logp(struct elog_t* self, const char* subsystem, const int severity, const char* payload);
```
This is also a wrapper for `elog_log` which omits the `message` field instead.

Returns 1 if message was sent successfully, otherwise 0.

